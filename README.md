# homebridge-ics1000 #

A homebridge plugin that controls 'KlikAanKlikUit' modules via 'ICS-1000'.

## Installation ##

1. Install Homebridge using: npm install -g homebridge
2. Install this plugin using: npm install -g homebridge-ics1000
3. Update your Homebridge config.json using the sample below.

## Configuration ##


```
{
  "accessory": "ICS1000-Dimmer",
  "name": "Dimmer1",
  "room": 1,
  "device": 1		
},
{
  "accessory": "ICS1000-OnOff",
  "name": "Lamp1",
  "room": 2,
  "device": 1		
},
```

Fields:

* **accessory**: Must be either "ICS1000-Dimmer" or "ICS1000-OnOff".

* **name**: User defined name of the device (will be used for logging).

* **room**: Room number as defined in the ICS1000 configuration.

* **device**: Device number as defined in the ICS1000 configuration.
